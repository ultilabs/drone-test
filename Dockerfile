FROM golang:1.5

ENTRYPOINT ["go"]

ADD .

CMD ["run", "main.go"]
