package main

import (
	"log"
	"time"
)

func main() {
	log.Println("I am a program that is running")

	time.Sleep(3 * time.Second)

	log.Println("I have finished running. Goodbye cruel Wor-")
}
